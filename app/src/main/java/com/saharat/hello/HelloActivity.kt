package com.saharat.hello

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.saharat.hello.databinding.ActivityHelloBinding
import com.saharat.hello.databinding.ActivityMainBinding


class HelloActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHelloBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHelloBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val name = intent?.extras?.getSerializable("name").toString()
        binding.name.text = name

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}